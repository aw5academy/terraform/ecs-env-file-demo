resource "aws_security_group" "alb" {
  description = "${local.app-name}-alb"
  name        = "${local.app-name}-alb"
  tags = {
    Name = "${local.app-name}-alb"
  }
  vpc_id = data.aws_vpc.default.id
}

data "external" "ifconfig" {
  program = ["bash", "${path.module}/ifconfig.sh"]
}

resource "aws_security_group_rule" "alb-inbound" {
  cidr_blocks              = [data.external.ifconfig.result["public_ip"]]
  description              = "Allowing HTTP access"
  from_port                = "80"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.alb.id
  to_port                  = "80"
  type                     = "ingress"
}

resource "aws_security_group_rule" "alb-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.alb.id
  to_port           = 0
  type              = "egress"
}

resource "aws_security_group" "ecs" {
  description = "${local.app-name}-ecs"
  name        = "${local.app-name}-ecs"
  tags = {
    Name = "${local.app-name}-ecs"
  }
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group_rule" "ecs-alb" {
  description              = "Allow HTTP traffic from ALB."
  from_port                = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ecs.id
  source_security_group_id = aws_security_group.alb.id
  to_port                  = 80
  type                     = "ingress"
}

resource "aws_security_group_rule" "ecs-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.ecs.id
  to_port           = 0
  type              = "egress"
}
