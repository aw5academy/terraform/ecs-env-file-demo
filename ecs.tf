resource "aws_ecs_cluster" "app" {
  name = local.app-name
  tags = {
    Name = local.app-name
  }
}

resource "aws_ecs_service" "app" {
  name                              = local.app-name
  cluster                           = aws_ecs_cluster.app.id
  task_definition                   = aws_ecs_task_definition.app.arn
  desired_count                     = "1"
  health_check_grace_period_seconds = "120"
  launch_type                       = "FARGATE"
  platform_version                  = "1.4.0"
  depends_on                        = [aws_lb.ecs]
  deployment_controller {
    type = "CODE_DEPLOY"
  }
  lifecycle {
    ignore_changes = [
      task_definition,
      load_balancer
    ]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.ecs["blue"].arn
    container_name   = local.app-name
    container_port   = "80"
  }
  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.ecs.id]
    subnets          = data.aws_subnet_ids.default.ids
  }
  tags = {
    Name = local.app-name
  }
}
