################################################
# ECS TASKS ROLE
################################################
data "aws_iam_policy_document" "ecs-tasks" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs-tasks" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "${local.app-name}-ecs"
  tags = {
    Name = "${local.app-name}-ecs"
  }
}

resource "aws_iam_role_policy" "ecs-tasks" {
  name   = "ecs-tasks"
  policy = templatefile("${path.module}/templates/ecs-tasks-policy.json.tpl", { bucket_arn = aws_s3_bucket.app.arn})
  role   = aws_iam_role.ecs-tasks.id
}

data "aws_iam_policy" "amazon-ecs-task-exec" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "amazon-ecs-task-exec" {
  policy_arn = data.aws_iam_policy.amazon-ecs-task-exec.arn
  role       = aws_iam_role.ecs-tasks.name
}

################################################
# CODEDEPLOY ROLE
################################################
data "aws_iam_policy" "codedeploy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}

data "aws_iam_policy_document" "codedeploy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codedeploy" {
  assume_role_policy = data.aws_iam_policy_document.codedeploy.json
  name               = "${local.app-name}-codedeploy"
  tags = {
    Name = "${local.app-name}-codedeploy"
  }
}

resource "aws_iam_role_policy_attachment" "codedeploy" {
  policy_arn = data.aws_iam_policy.codedeploy.arn
  role       = aws_iam_role.codedeploy.name
}

resource "aws_iam_role_policy" "codedeploy" {
  name   = "codedeploy"
  policy = templatefile("${path.module}/templates/codedeploy-policy.json.tpl", { bucket_arn = aws_s3_bucket.app.arn})
  role   = aws_iam_role.codedeploy.id
}

################################################
# CODEBUILD ROLE
################################################
data "aws_iam_policy_document" "codebuild" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  assume_role_policy = data.aws_iam_policy_document.codebuild.json
  name               = "${local.app-name}-codebuild"
  tags = {
    Name = "${local.app-name}-codebuild"
  }
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "codebuild"
  policy = templatefile("${path.module}/templates/codebuild-policy.json.tpl", { 
             aws_account_id = data.aws_caller_identity.current.account_id,
             bucket_arn     = aws_s3_bucket.app.arn,
             region         = "us-east-1"
           })
  role   = aws_iam_role.codebuild.id
}

################################################
# CODEPIPELINE ROLE
################################################
data "aws_iam_policy_document" "codepipeline" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codepipeline" {
  assume_role_policy = data.aws_iam_policy_document.codepipeline.json
  name               = "${local.app-name}-codepipeline"
  tags = {
    Name = "${local.app-name}-codepipeline"
  }
}

resource "aws_iam_role_policy" "codepipeline" {
  name   = "codepipeline"
  policy = templatefile("${path.module}/templates/codepipeline-policy.json.tpl", {})
  role   = aws_iam_role.codepipeline.id
}
