[
  {
    "name": "${app}",
    "image": "${accountId}.dkr.ecr.us-east-1.amazonaws.com/${app}:latest",
    "environmentFiles": [
      {
        "type": "s3",
        "value": "${cfgFileArn}"
      }
    ],
    "essential": true,
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "/aws/ecs/${app}",
            "awslogs-region": "us-east-1",
            "awslogs-stream-prefix": "stream"
        }
    },
    "portMappings": [
      {
        "protocol": "tcp",
        "containerPort": 80
      }
    ]
  }
]
