resource "local_file" "deploy-test" {
  content  = templatefile("${path.module}/templates/deployment-tester.html.tpl", { albUrl = aws_lb.ecs.dns_name})
  filename = "${path.module}/deployment-tester.html"
}
