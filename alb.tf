resource "aws_lb" "ecs" {
  internal           = false
  load_balancer_type = "application"
  name               = local.app-name
  security_groups    = [aws_security_group.alb.id]
  subnets            = data.aws_subnet_ids.default.ids
  tags = {
    Name = local.app-name
  }
}

resource "aws_lb_listener" "ecs" {
  default_action {
    target_group_arn = aws_lb_target_group.ecs["blue"].arn
    type             = "forward"
  }
  lifecycle {
    ignore_changes = [
      default_action
    ]
  }
  load_balancer_arn = aws_lb.ecs.arn
  port              = "80"
  protocol          = "HTTP"
}

resource "aws_lb_target_group" "ecs" {
  for_each             = toset(["blue", "green"])
  name                 = "${local.app-name}-${each.value}"
  port                 = "80"
  protocol             = "HTTP"
  vpc_id               = data.aws_vpc.default.id
  target_type          = "ip"
  health_check {
    healthy_threshold   = "2"
    interval            = "30"
    matcher             = "200"
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
    timeout             = "5"
    unhealthy_threshold = "2"
  }
  tags = {
    Name = "${local.app-name}-${each.value}"
  }
}
