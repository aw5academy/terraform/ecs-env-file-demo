resource "random_string" "random" {
  length  = 12
  special = false
  upper   = false
  number  = false
}

resource "aws_s3_bucket" "app" {
  bucket        = "${local.app-name}-${random_string.random.result}"
  acl           = "private"
  force_destroy = true
  tags = {
    Name = "${local.app-name}-${random_string.random.result}"
  }
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_object" "cfg" {
  bucket = aws_s3_bucket.app.id
  key    = "cfg/placeholder.env"
  source = "placeholder.env"
  etag   = filemd5("placeholder.env")
}
