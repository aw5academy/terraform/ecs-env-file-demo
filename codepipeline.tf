resource "aws_codepipeline" "ecs" {
  name     = local.app-name
  role_arn = aws_iam_role.codepipeline.arn
  artifact_store {
    location = aws_s3_bucket.app.id
    type     = "S3"
  }
  stage {
    name = "Source"
    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["SourceArti"]
      configuration = {
        RepositoryName = aws_codecommit_repository.app.repository_name
        BranchName     = "master"
      }
    }
  }
  stage {
    name = "Build"
    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["SourceArti"]
      output_artifacts = ["BuildArti"]
      version          = "1"
      configuration = {
        ProjectName = aws_codebuild_project.ecs.name
      }
    }
  }
  stage {
    name = "Deploy"
    action {
      name             = "Deploy"
      category         = "Deploy"
      owner            = "AWS"
      provider         = "CodeDeployToECS"
      input_artifacts  = ["BuildArti"]
      version          = "1"
      configuration = {
        AppSpecTemplateArtifact        = "BuildArti"
        AppSpecTemplatePath            = "appspec.yaml"
        ApplicationName                = aws_codedeploy_app.ecs.name
        DeploymentGroupName            = aws_codedeploy_deployment_group.ecs.deployment_group_name
        TaskDefinitionTemplateArtifact = "BuildArti"
        TaskDefinitionTemplatePath     = "taskdef.json"
      }
    }
  }
  tags = {
    Name = local.app-name
  }
}
