resource "aws_ecr_repository" "app" {
  name = local.app-name
  tags = {
    Name = local.app-name
  }
}
