resource "aws_ecs_task_definition" "app" {
  family                   = local.app-name
  container_definitions    = templatefile("${path.module}/templates/container-definitions.json.tpl", {
                               accountId  = data.aws_caller_identity.current.account_id,
                               app        = local.app-name,
                               cfgFileArn = "arn:aws:s3:::${aws_s3_bucket.app.id}/${aws_s3_bucket_object.cfg.key}"
                             })
  execution_role_arn       = aws_iam_role.ecs-tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "256"
  memory                   = "512"
}
