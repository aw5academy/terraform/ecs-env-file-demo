resource "aws_codebuild_project" "ecs" {
  artifacts {
    type = "S3"
    location       = aws_s3_bucket.app.id
    name           = "artifacts.zip"
    namespace_type = "BUILD_ID"
    packaging      = "ZIP"
    path           = "/codebuild/"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    environment_variable {
      name = "AWS_ACCOUNT_ID"
      value = data.aws_caller_identity.current.account_id
    }
    environment_variable {
      name = "TASK_DEFINITION_ARN"
      value = aws_ecs_task_definition.app.arn
    }
    environment_variable {
      name = "APP"
      value = local.app-name
    }
    environment_variable {
      name = "EXECUTION_ROLE_ARN"
      value = aws_iam_role.ecs-tasks.arn
    }
    environment_variable {
      name = "APP_BUCKET"
      value = aws_s3_bucket.app.id
    }
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  logs_config {
    cloudwatch_logs {
      group_name = aws_cloudwatch_log_group.codebuild.name
    }
  }
  name          = local.app-name
  service_role  = aws_iam_role.codebuild.arn
  source {
    type            = "S3"
    location        = "${aws_s3_bucket.app.id}/codebuild/source.zip"
  }
  tags = {
    Name = local.app-name
  }
}
