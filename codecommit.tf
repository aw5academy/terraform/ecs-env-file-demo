resource "aws_codecommit_repository" "app" {
  repository_name = local.app-name
  tags = {
    Name = local.app-name
  }
}
