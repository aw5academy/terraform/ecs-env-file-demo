resource "aws_codedeploy_app" "ecs" {
  compute_platform = "ECS"
  name             = local.app-name
}

resource "aws_codedeploy_deployment_config" "app" {
  deployment_config_name = local.app-name
  compute_platform       = "ECS"
  traffic_routing_config {
    type = "TimeBasedCanary"
    time_based_canary {
      interval   = 5
      percentage = 20
    }
  }
}

resource "aws_codedeploy_deployment_group" "ecs" {
  app_name               = aws_codedeploy_app.ecs.name
  deployment_config_name = aws_codedeploy_deployment_config.app.id
  deployment_group_name  = local.app-name
  service_role_arn       = aws_iam_role.codedeploy.arn
  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }
  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout    = "CONTINUE_DEPLOYMENT"
    }
    terminate_blue_instances_on_deployment_success {
      action                           = "TERMINATE"
      termination_wait_time_in_minutes = 0
    }
  }
  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }
  ecs_service {
    cluster_name = aws_ecs_cluster.app.name
    service_name = aws_ecs_service.app.name
  }
  load_balancer_info {
    target_group_pair_info {
      prod_traffic_route {
        listener_arns = [aws_lb_listener.ecs.arn]
      }
      target_group {
        name = aws_lb_target_group.ecs["blue"].name
      }
      target_group {
        name = aws_lb_target_group.ecs["green"].name
      }
    }
  }
}
